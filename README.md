This project intends to estimate the scores of players in the Draftkings' fantasy sports using third-party analyst data, and build teams to maximize the chances of winning.

This project has been discontinued after the supplier of DK results became unable to continue, and we are unable to obtain the data on our own since DK is not allowed to legally operate in Spain.
