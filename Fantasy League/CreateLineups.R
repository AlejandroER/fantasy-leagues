source("ModeloOptim.R")



{start_Time <- Sys.time()
Test <- MakeFootballLineups(GoaliesData,NGoalies,PlayersData,NPlayers,Budget,Teams,MultiPos,
                                Solver="glpk",Overlap,MaxLineups=80,Type="Type3")
end_Time   <- Sys.time()
RunTime.glpk <- end_Time - start_Time
}

start_Time <- Sys.time()
Test <- MakeFootballLineups(GoaliesData,NGoalies,PlayersData,NPlayers,Budget,Teams,MultiPos,
                            Solver="lpsolve",Overlap,MaxLineups=80,Type="Type3")
end_Time   <- Sys.time()
RunTime.lpSolve <- end_Time - start_Time

{start_Time <- Sys.time()
Test <- MakeFootballLineups(GoaliesData,NGoalies,PlayersData,NPlayers,Budget,Teams,MultiPos,
                            Solver="cplex",Overlap,MaxLineups=80,Type="Type3")
end_Time   <- Sys.time()
RunTime.cplex <- end_Time - start_Time
}

{start_Time <- Sys.time()
  Test <- MakeFootballLineups(GoaliesData,NGoalies,PlayersData,NPlayers,Budget,Teams,MultiPos,
                              Solver="gurobi",Overlap,MaxLineups=80,Type="Type3")
  end_Time   <- Sys.time()
  RunTime.cplex <- end_Time - start_Time
}

Testing_Lineups <- MakeTestFootballLineups(GoaliesData,NGoalies,PlayersData,NPlayers,Budget,Teams,MultiPos,
                                           Solver="cplex",MaxLineups=80)

